## Synopsis

A web application that performs the following functions: 
- Capture a person’s name and a number 
- Convert this number into words 
- Render this name and number (as words) as a web page

## Example

###Input:
Name: John Smith 
Number: 123.45
###Output:
John Smith “ONE HUNDRED AND TWENTY THREE DOLLARS AND FORTY FIVE CENTS”

## Installation

After pulling the repo, you should just open the solution file then restore Nuget packages. Run Debug - Start Debugging (or F5) from Visual Studio.

## API Reference

/api/convert?name={name}&number={number}

## Tests

Use any nUnit test runner to run tests
e.g. https://github.com/nunit/docs/wiki/Console-Runner
