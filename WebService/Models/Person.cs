﻿namespace WebService.Models
{
    /// <summary>
    ///     Person class object
    /// </summary>
    public class Person
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}