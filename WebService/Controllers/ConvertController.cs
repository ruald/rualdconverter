﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Http;
using WebService.Models;

namespace WebService.Controllers
{
    [SuppressMessage("ReSharper", "RedundantAssignment")]
    [SuppressMessage("ReSharper", "InvalidXmlDocComment")]
    public class ConvertController : ApiController
    {
        private const string Hundred = "HUNDRED";
        private const string And = "AND";
        private const string Dollars = "DOLLARS";
        private const string Cents = "CENTS";

        private static readonly string[] BelowTwenty =
        {
            "", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE",
            "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"
        };

        private static readonly string[] DoubleDigits =
            {"TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"};

        private static readonly string[] Groups = {"", " THOUSAND", " MILLION", " BILLION"};

        /// <summary>
        ///     This method captures a name and number then converts the number into words
        ///     Format: /api/convert?name={name}&number={number}
        /// </summary>
        /// <param name="name">Name of the person you need to print on screen</param>
        /// <param name="number">Number that will be converted into words</param>
        /// <returns>The name and number converted into words</returns>
        public Person GetDetails(string name, decimal number)
        {
            //Need to split out the dollars and cents
            var dollarValue = Math.Floor(number);
            var centValue = number - dollarValue;
            var finalCent = string.Empty;
            if (centValue > 0)
            {
                var centInt = (int) (centValue * 100);
                finalCent = ConvertIntToWords(centInt, string.Empty, 0);
            }
            else
            {
                finalCent = "ZERO";
            }

            var dollarInt = 0;
            if (number <= int.MaxValue)
            {
                dollarInt = (int) number;
            }
            else
            {
                throw new ArgumentException("Number exceeded maximum allowed", nameof(number));
            }

            var finalValue = $"{ConvertIntToWords(dollarInt, string.Empty, 0)} {Dollars} {And} {finalCent} {Cents}";

            var person = new Person {Name = name, Number = finalValue};
            return person;
        }

        /// <summary>
        ///     Main method that converts the number into words
        /// </summary>
        /// <param name="number">Number that needs to be converted</param>
        /// <param name="leftDigits">The converted number that will be used on a subsequent iteration</param>
        /// <param name="groupLevel">Used to differentiate between the value used inside the "groups" array</param>
        /// <returns></returns>
        private static string ConvertIntToWords(int number, string leftDigits, int groupLevel)
        {
            if (number == 0) return leftDigits;

            //combine any previously converted number to the string
            var intString = leftDigits;

            if (intString.Length > 0) intString += " ";

            //straight-forward mapping on digits to words
            if (number < 20)
            {
                intString += BelowTwenty[number];
            }
            else if (number < 100)
            {
                var remainder = number % 10; //get the remainder that will be used in the subsequent iteration
                intString += ConvertIntToWords(remainder, DoubleDigits[number / 10 - 2], 0);
            }
            else if (number < 1000)
            {
                var remainder = number % 100; //get the remainder that will be used in the subsequent iteration
                intString += ConvertIntToWords(remainder, $"{BelowTwenty[number / 100]} {Hundred} {And}", 0);
            }
            else
            {
                var remainder = number % 1000; //get the remainder that will be used in the subsequent iteration
                intString += ConvertIntToWords(remainder, ConvertIntToWords(number / 1000, string.Empty, groupLevel + 1), 0);
                if (remainder == 0) return intString;
            }

            return intString + Groups[groupLevel];
        }
    }
}