﻿using System.Web.Mvc;

namespace WebService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Ruald Converter Test";

            return View();
        }
    }
}