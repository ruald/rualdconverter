﻿using NUnit.Framework;
using WebService.Controllers;
using WebService.Models;

namespace WebService.Tests.Controllers
{
    [TestFixture]
    public class ConvertControllerTest
    {
        [SetUp]
        public virtual void SetUp()
        {
            _person = new Person
                {Name = "John Smith", Number = "ONE HUNDRED AND TWENTY THREE DOLLARS AND FORTY FIVE CENTS"};
        }

        private Person _person = new Person();

        [Test]
        public void Convert_ShouldBeEqual_WhenCorrectParamsPassed()
        {
            // Arrange
            var controller = new ConvertController();

            // Act
            var result = controller.GetDetails("John Smith", 123.45m);

            // Assert
            Assert.AreEqual(_person.Number, result.Number);
        }
    }
}