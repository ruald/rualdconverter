﻿using System.Web.Mvc;
using NUnit.Framework;
using WebService.Controllers;

namespace WebService.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        private const string PageTitle = "Ruald Converter Test";

        [Test]
        public void Index()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(PageTitle, result.ViewBag.Title);
        }
    }
}